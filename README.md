# Tic-Tac-Toe

Access the live page at: [https://yoadrian.gitlab.io/ttt/](https://yoadrian.gitlab.io/ttt/)

## Project learning-points
- using modules and the revealing module pattern
- using factory functions as an alternative to object constructors
- private variables and functions (related to modules)
- using IIFEs
- player vs computer mode using the minimax algorithm
